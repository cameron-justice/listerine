import unittest
import listerine/lists
import random, os

randomize()

suite "List tests":
  setup:
    var
      testPath = joinPath(getTempDir(), "test.json")
      handler = newListHandler(testPath)
      testList: ListerineList

  test "Samples Correctly":
    testList.contents = @["1", "2", "3", "4", "5"]
    var
      sampleCount = 3
      samples = testList.sampleFromList(sampleCount)
    check samples.len == sampleCount
