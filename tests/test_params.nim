import unittest
import listerine/params

suite "Param Handler Tests":
  setup:
    var handler: ParamHandler

  test "Correctly changes params":
    let params = @["param1", "param2", "param3"]
    handler.changeParams(params)
    check handler.params == params
    check handler.count == params.len
    check handler.index == 0

  test "Correctly identifies flags":
    handler.changeParams(@["-flag1", "-flag2"])
    check isFlag("-f") == true
    check isFlag("f") == false
    check handler.getAllFlags().len == 2
