# Package

version     = "0.0.1"
author      = "Kenalia"
description = "List Handling CLI Tool"
license     = "MIT"
srcDir      = "src"
skipDirs    = @["tests"]

requires "nim >= 1.0.0"
