import os, streams, sequtils, strutils, sugar
import listerine/[params, lists]

proc printHelp(): void =
  echo """Usage: list-handler [command] [options] [inputs]

Listerine: a listing system.
https://gitlab.com/cameron-justice/listerine

Commands:
    Management   Inputs
    -------------------
    create       [name]           Create a new list
    delete       [name]           Delete a list
    add          [name] [items]   Add items to list, seperated by spaces
    remove       [name] [items]   Remove items from list, seperated by spaces

    Utility      Inputs
    -------------------
    sample       [name] [count]   Picks [count] samples, outputs and removes from list
    unique       [name]           Filter duplicates from list

Options:
    -f, --force   Force the action, skipping any confirmations.
    -s, --safe    Safe Mode, requires confirmation before any change

"""

proc quitOnBadInput(): void =
  echo "Bad Input"
  printHelp()

proc main() =
  if paramCount() < 1:
    quitOnBadInput()
  var paramHandler = setupParams()
  var listHandler = newListHandler("test.json")
  var source = ListerineList(key: "test", contents: @["1", "2", "3", "4"])
  echo source.sampleFromList(2)
  # 1 command
  # N amount of flags
  # M amount of inputs


main()
