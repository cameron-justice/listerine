import os, streams, sequtils, strutils, strformat, sugar

type ParamHandler* = object
    count*: int
    params*: seq[string]
    index*: int

proc changeParams*(handler: var ParamHandler, newParams: seq[string]) =
  handler.params = newParams
  handler.count = newParams.len
  handler.index = 0

proc setupParams*(): ParamHandler =
  var ph = ParamHandler(count: paramCount(), index: 0)
  for i in 1 .. ph.count:
    var ps = paramStr(i)
    echo ps
    ph.params.add(paramStr(i))
  return ph

proc getNextParam*[T](handler: var ParamHandler, parse: proc(x: string): T): T =
  if handler.index < handler.count:
    try:
      result = parse(handler.params[handler.index])
      handler.index = handler.index+1
    except ValueError:
      let
        e = getCurrentException()
        msg = getCurrentExceptionMsg()
      echo "Error: Can not convert argument. Stack trace: \n", msg
  else:
      raise newException(IOError, "Attempt to access outside parameter list")

proc keepStr*(x: string): string = x

proc isFlag*(toCheck: string): bool =
  return @[
    toCheck.len >= 2,
    '-' in toCheck,
    toCheck[0] == '-'
    ].all((x: bool) => x)

proc getAllFlags*(handler: var ParamHandler): seq[string] =
  var flags = newSeq[string]()
  while handler.index < handler.count:
    let param = handler.getNextParam(keepStr)
    if not isFlag(param):
      dec handler.index
      break
    else:
      flags.add(param)
  return flags
