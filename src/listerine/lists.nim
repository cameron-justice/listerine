import strformat, os, options, sequtils, random, sets

randomize()

# We need an object that can:
#   1. Control a file of soem format that has data about a lsit
#   2. Managing creating, editing, and manipulating the data in that file
#   3. Use a diffing technique to not ovoerwrite the entire file on individual changes.

# What does it need to keep?
# Obviously needs the file, but should the file read/write be in another module?

# What should our file format look like? JSON would be easy
# JSON is probably most readable so let's start there

# Ctrl+V to visual block, select all lines, Shift+I, add or remove comment

# TODO: Verification file, changes are appended so it can be verified outside

# This is the type reference for anything that pulls or inputs to a list
type ListItem = string

# This is the type reference for identifying a list
type ListKey = string

type ListAction = enum
  laCreate, laInsert, laRemove

type ListerineList* = object
    key*: string
    options: seq[string]
    contents*: seq[ListItem]

type ListChange = object
    affected: ListKey
    action: ListAction
    handler: proc(s: ListChange)

# Trying to handle a verification and tracking. The ListChange type should be used to confirm a
# history of changes to write to the validation file, but how to handle a variety of actions?

type ListHandler* = object
    stableLists: seq[ListerineList]
    unstableLists: seq[ListerineList]
    changes: seq[ListChange]


# proc diff(self: ListerineList, other: ListerineList): ListerineList =

# Fisher-Yates Sample
#
proc sampleFromList*(source: ListerineList, sampleCount: int): seq[ListItem] =
  var
    indices = toSeq(0 ..< source.contents.len)
    samples = newSeq[int](sampleCount)

  for i in 0 ..< sampleCount:
    var r = rand(indices.len-1)
    result.add(source.contents[indices[r]])
    indices.delete(r)

proc readListFile*(handler: ListHandler, filePath: string, verifiedExists: bool = false): ListerineList =
#  if not (verifiedExists or fileExists(filePath)):
#    raise newException(IOError, fmt"Attempted to open {filePath}, but it does not exist.")
  echo filePath

#proc writeListFile*(handler: ListHandler) =


proc newListHandler*(filePath: string): ListHandler =
  var handler: ListHandler

  # We need to read the file and get the data (if it already exists)
  if fileExists(filePath):
    echo handler.readListFile(filePath, verifiedExists=true)
  else:
    echo filePath
  return handler
